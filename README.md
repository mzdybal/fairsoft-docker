# Docker image for FairMUonE CI

Docker image to be used for testing FairMUonE. Contains precompiled `FairSoft` in `/FairSoft/install`. Build process takes about 1 hour. Time limit for CI jobs in this project needs to increased from default 1 hour.

# Usage

Commit with tag to build an image. Image is stored in the registry under the name followed by the commit tag.
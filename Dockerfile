FROM gitlab-registry.cern.ch/eos/gitlab-eos/alma9:latest as build

RUN dnf install -y git
RUN git clone -b master https://github.com/miloszz/FairSoft.git
WORKDIR /FairSoft
RUN bash legacy/setup-almalinux.sh
RUN mkdir build
WORKDIR /FairSoft/build
RUN cmake -S .. -B . -C ../FairSoftConfig.cmake
RUN cmake --build . -j$(nproc)
WORKDIR /
RUN rm -rf /FairSoft/build

